#!/usr/bin/env node

var Path = require('path'),
    FS = require('fs'),
    yargs = require('yargs'),
    pep  = require('pep');

var EXT_PREFIX = '.';
var EXT = 'tpl';
var TPL_EXTNAME = EXT_PREFIX + EXT;
var CONTRIB_CONFIG_FILENAME = 'contribulator.json';
var CONTRIB_CONFIG_PATH = Path.join(process.cwd(), CONTRIB_CONFIG_FILENAME);
var CONTRIB_TPL_PATH = Path.join(__dirname, '..', 'templates', 'CONTRIBUTING.md.tpl');
var CONTRIB_CONFIG_NOT_FOUND = 'Could not find your ' + CONTRIB_CONFIG_FILENAME;

var argv = yargs
  .alias('c', 'config')
  .alias('t', 'template')
  .default('c', CONTRIB_CONFIG_PATH)
  .default('t', CONTRIB_TPL_PATH)
  .usage('Usage: $0 [options] (a contribulator.json must be present)')
  .help('h')
  .alias('h', 'help')
  .example('$0 -c config.json', 'Pass a custom config path')
  .example('$0 -t CONTRIB.md.tpl', 'Pass a custom markdown template')
  .argv;

var OUT_PREFIX = '[' + argv.$0 + '] ';

function filenameWithExtnameRemoved(filename){
  var extname = Path.extname(filename);
  return filename.replace(extname,'');
}

function templateWithExtensionRemoved(pathToFile, ext){
  ext = ext || EXT;

  var filename = Path.basename(pathToFile);
  var extname  = Path.extname(filename);
  if(extname !== TPL_EXTNAME){
    return;
  }

  if(!FS.existsSync(pathToFile)){
    return;
  }

  var newFilename = filenameWithExtnameRemoved(filename);
  return newFilename;
}

function getOutputFilePath(){
  var outputFilename = templateWithExtensionRemoved(argv.template);
  return Path.join(process.cwd(), outputFilename);
}

function getPopulatedTemplate(){
  var outputFilePath = getOutputFilePath();
  var tplContents = FS.readFileSync(argv.template).toString();

  var config;
  try{
    config = require(argv.config);
  }catch(e){
    console.error(OUT_PREFIX + CONTRIB_CONFIG_NOT_FOUND);
  }
  if(config){
    return pep(tplContents, config);
  }
  return;
}

function writePopulatedTemplate(options){
  if(options){
    argv = argv || {};
    Object.keys(options).forEach(function(key){
      argv[key] = options[key];
    });
  }
  var populatedTemplate = getPopulatedTemplate();
  if(!populatedTemplate){
    return;
  }
  var outputFilePath = getOutputFilePath();
  FS.writeFileSync(outputFilePath, populatedTemplate);
  return outputFilePath;
}

module.exports = {
  TPL_EXTNAME: TPL_EXTNAME,
  CONTRIB_CONFIG_NOT_FOUND: CONTRIB_CONFIG_NOT_FOUND,
  write: writePopulatedTemplate
};

if(!module.parent && !argv.help){
  var pathWrittenTo = writePopulatedTemplate();
  pathWrittenTo && console.info(OUT_PREFIX + ' Wrote to ' + Path.relative(process.cwd(), pathWrittenTo));
}
