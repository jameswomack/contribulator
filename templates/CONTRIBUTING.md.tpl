# Contributing to {:module_name}

:+1::tada: First off, thanks for taking the time to contribute! :tada::+1:

The following is a set of guidelines for contributing to {:module_name} and its packages,
which are hosted in the [{:organization_name}]({:organization_url}) on {:organization_host}.
These are just guidelines, not rules, use your best judgment and feel free to
propose changes to this document in a pull request.

## Submitting Issues

* You can create an issue [here]({:issue_creation_url}), but
  before doing that please read the notes below on debugging and submitting issues,
  and include as many details as possible with your report.
* Check the [debugging guide]({:debugging_url}) for tips
  on debugging. You might be able to find the cause of the problem and fix
  things yourself.
* Include the version of {:module_name} you are using and the OS.
* Include screenshots and animated GIFs whenever possible; they are immensely
  helpful.
* Include the behavior you expected and other places you've seen that behavior
* Perform a cursory search to see if a similar issue has already been submitted.
* Please setup a profile picture to make yourself recognizable and so we can 
  all get to know each other better.

## Pull Requests

* Include screenshots and animated GIFs in your pull request whenever possible.
* Follow the [JavaScript]({:javascript_styleguide_url}) styleguide
* Include thoughtfully-worded, well-structured
  [{:test_framework_name}]({:test_framework_url}) specs in the `./test/unit/specs` folder. Run them using `npm test`. See the [Specs Styleguide](#specs-styleguide) below.
* Document new code based on the
  [Documentation Styleguide](#documentation-styleguide)
* End files with a newline.
* Place requires in the following order:
    * Built in Node Modules (such as `path`)
    * NPM Modules
    * Local Modules (using relative paths)
* Place class properties in the following order:
    * Class methods and properties
    * Instance methods and properties
* Avoid platform-dependent code:
    * Use `require('fs-plus').getHomeDirectory()` to get the home directory.
    * Use `Path.join()` to concatenate filenames.
    * Use `OS.tmpdir()` rather than `/tmp` when you need to reference the
      temporary directory.
* Using a plain `return` when returning explicitly at the end of a function.
    * Not `return null`, `return undefined`, `null`, or `undefined`

## Git Commit Messages

* Use the present tense ("Add feature" not "Added feature")
* Use the imperative mood ("Move cursor to..." not "Moves cursor to...")
* Limit the first line to 72 characters or less
* Reference issues and pull requests liberally
* Consider starting the commit message with an applicable emoji:
    * :art: `:art:` when improving the format/structure of the code
    * :racehorse: `:racehorse:` when improving performance
    * :non-potable_water: `:non-potable_water:` when plugging memory leaks
    * :memo: `:memo:` when writing docs
    * :penguin: `:penguin:` when fixing something on Linux
    * :apple: `:apple:` when fixing something on Mac OS
    * :checkered_flag: `:checkered_flag:` when fixing something on Windows
    * :bug: `:bug:` when fixing a bug
    * :fire: `:fire:` when removing code or files
    * :green_heart: `:green_heart:` when fixing the CI build
    * :white_check_mark: `:white_check_mark:` when adding tests
    * :lock: `:lock:` when dealing with security
    * :arrow_up: `:arrow_up:` when upgrading dependencies
    * :arrow_down: `:arrow_down:` when downgrading dependencies
    * :shirt: `:shirt:` when removing linter warnings

## Specs Styleguide

- Include thoughtfully-worded, well-structured
  [{:test_framework_name}]({test_framework_url}) specs in the `./test/unit/specs` folder.
- treat `describe` as a noun or situation.
- treat `it` as a statement about state or how an operation changes state.

### Example

```coffee
describe 'a dog', ->
 it 'barks', ->
 # spec here
 describe 'when the dog is happy', ->
  it 'wags its tail', ->
  # spec here
```

## Documentation Styleguide

* Use [Markdown](https://daringfireball.net/projects/markdown).
* Reference methods and classes in markdown with the custom `{}` notation:
    * Reference classes with `{ClassName}`
    * Reference instance methods with `{ClassName::methodName}`
    * Reference class methods with `{ClassName.methodName}`

This CONTRIBUTING file is based on [Atom's](https://raw.githubusercontent.com/atom/atom/master/CONTRIBUTING.md)
