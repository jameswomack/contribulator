'use strict';

var FS      = require('fs'),
    Path    = require('path'),
    nixt    = require('nixt'),
    expect  = require('chai').expect,
    cpFile  = require('cp-file'),
    del     = require('del'),
    libDir  = Path.join(process.cwd(), 'lib'),
    contrib = require(Path.join(libDir, 'contribulator')),
    runCmd  = 'node contribulator.js',
    mdName  = 'CONTRIBUTING.md';

function run(){
  return nixt()
    .cwd(libDir)
    .run(runCmd)
}

describe('contribulator', function(){
  var mdPath  = process.cwd() + '/lib/' + mdName;

  describe('contribulator sans config', function(){
    it('should output an error if no JSON is present', function(onEnd){
      run()
        .stderr(contrib.CONTRIB_CONFIG_NOT_FOUND)
        .end(onEnd)
    })
  })

  describe('contribulator with config', function(){
    var contrib_config = {
      src: process.cwd() + '/templates/contribulator.json',
      dest: process.cwd() + '/lib/contribulator.json'
    }

    before(function(onEnd){
      cpFile(contrib_config.src, contrib_config.dest, onEnd)
    })

    after(function(onEnd){
      del([contrib_config.dest, mdPath], {force:true}, onEnd)
    })

    it('should output an path written to if JSON is present', function(onEnd){
      run()
        .stdout('Wrote to CONTRIBUTING.md')
        .end(onEnd)
    })

    it('should inject from the config into the markdown file', function(onEnd){
      run()
        .stdout('Wrote to CONTRIBUTING.md')
        .end(function(){
          var config = require(contrib_config.dest)
          var md = FS.readFileSync(mdPath).toString()
          Object.keys(config).forEach(function(key){
            var value = config[key]
            expect(md.indexOf(value)).to.be.above(0)
          })
          onEnd()
        })
    })
  })

  describe('contribulator interface', function(){
    it('should expose only `write` & `CONTRIB_CONFIG_NOT_FOUND`', function(){
      expect(Object.keys(contrib)).to.eql(['TPL_EXTNAME','CONTRIB_CONFIG_NOT_FOUND','write'])
    })
  })

  describe('contribulator programmatic use', function(){
    var mdTplPath = process.cwd() + '/templates/' + mdName + contrib.TPL_EXTNAME;
    var mdPath = process.cwd() + '/' + mdName;
    var contrib_config = {
      src: process.cwd() + '/templates/contribulator.json',
      dest: process.cwd() + '/contribulator.json'
    }

    before(function(onEnd){
      cpFile(contrib_config.src, contrib_config.dest, onEnd)
    })

    after(function(onEnd){
      del([contrib_config.dest, mdPath], {force:true}, onEnd)
    })

    it('should accept options', function(){
      contrib.write({
        config: process.cwd() + '/test/fixtures/foo.json',
        template: mdTplPath
      })
      var config = require(contrib_config.dest)
      var md = FS.readFileSync(mdPath).toString()
      expect(md.indexOf('http://jasmine.github.io/')).to.be.above(0);
    })
  })
})
